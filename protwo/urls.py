"""protwo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from apptwo import views
from apptwo.views import UserCreate, UserDelete, UserUpdate , ProductList ,ProducctDetail
 

urlpatterns = [
    path('' ,views.index,name='index'),
    path('admin/', admin.site.urls),
    path('formpage/' , views.user_form, name='form_name'),
    path('modelpage/', views.user_model_form,name='model_name'),
    path('auther/add/', UserCreate.as_view(), name='user-add'),
    path('author/<int:pk>/update/', UserUpdate.as_view(), name='author-update'),
    path('author/<int:pk>/delete/', UserDelete.as_view(), name='author-delete'),
    path('productlist/', ProductList.as_view()),
    path('detail/<int:pk>/', ProducctDetail.as_view(), name='author-detail'),
    
    # path('user/', include('apptwo.urls')),
]
