from  django import forms
from apptwo.models import Product, Cart, Category,User,Order

class UserForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField()
    text = forms.CharField()


class UserModelForm(forms.ModelForm):
    class Meta:
        model = User 
        fields ='__all__'   
    
class CategoryForm(forms.Form):
    title = forms.CharField()
    primaryCategory = forms.BooleanField()
      
class CategoryFormModel(forms.ModelForm):
    class Meta:
        model = Category 
        fields ='__all__'   
    


class ProductForm(forms.Form):
    name = forms.CharField()
    # category = forms.ForeignKey(CategoryForm, on_delete=models.CASCADE)
    # preview_text = forms.TextField()
    # detail_text = forms.TextField()
    price = forms.FloatField()      

class ProductFormModel(forms.ModelForm):
    class Meta:
        model = Product 
        fields ='__all__'   
    


class CartForm(forms.Form):
    # item = forms.ForeignKey(ProductForm, on_delete=models.CASCADE)
    quantity = forms.IntegerField()
    created = forms.DateTimeField()
         
class CartFormModel(forms.ModelForm):
    class Meta:
        model = Cart
        fields ='__all__'

class OrderForm(forms.Form):
    # orderitems = forms.ManyToManyField(Cart)
    # user = forms.ForeignKey(UserForm, on_delete=models.CASCADE)
    ordered = forms.BooleanField()
    created = forms.DateTimeField()

class OrderFormModel(forms.ModelForm):
    class Meta:
        model = Order
        fields ='__all__'