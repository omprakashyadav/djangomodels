
from django.urls import path
from apptwo.views import  UserCreate, UserDelete ,UserUpdate
from apptwo import views


urlpatterns = [
    path('product', views.product_view, name='product'),
    path('card', views.card_view, name='card'),
    path('order', views.order_view, name='order'),
    path('category' , views.category_view, name='category'),
    # path('user/add/', UserCreate.as_view(), name='user-add'),
    # path('user/<int:pk>/', UserDelete.as_view(), name='user-update'),
    # path('user/<int:pk>/delete/', UserUpdate.as_view(), name='user-delete'),
    # path('use' , views.user_form, name='use')
]
