from django.db import models

# Create your models here.


class Category(models.Model):
    title = models.CharField("title_name" , max_length=300 , db_column='title_name')
    primaryCategory = models.BooleanField(default=False)
    
    @property
    def _get_name(self):
            return self.title
    
    def _set_name(self, value):
            self.title = value
    def __str__(self):
        return self.title


class Product(models.Model):
    name = models.CharField(max_length=300)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    preview_text = models.TextField(max_length=200, verbose_name='Preview Text')
    detail_text = models.TextField(max_length=1000, verbose_name='Detail Text')
    price = models.FloatField()
    
    @property
    def _get_name(self):
            return self.name
    def _set_name(self, value):
            self.name = value 
    def __str__(self):
        return self.name



class Cart(models.Model):
    item = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True)
     
    @property  
    def _get_name(self):
            return self.quantity
    def _set_name(self, value):
            self.quantity = value   
    def __str__(self):
        return self.item  

class User(models.Model):
    username = models.CharField(max_length=300)
    email = models.EmailField(max_length=264 , unique=True)
    password = models.CharField(max_length=300)
    
    
    @property
    def _get_name(self):
            return self.username
  
    def _set_name(self, value):
            self.username = value
    
    
    def __str__(self):
        return self.email

class Order(models.Model):
    orderitems = models.ManyToManyField(Cart)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    
    @property
    def _get_name(self):
            return self.orderitems
    def _set_name(self, value):
            self.orderitems = value
    
    
    def __str__(self):
        return self.user.username
