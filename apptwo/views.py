from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView ,UpdateView
from apptwo.models import User , Cart , Category ,Order ,Product
from django.views.generic import ListView
from django.views.generic import DetailView
# from django.http import HttpResponse
from . import forms
# Create your views here.

class UserCreate(CreateView):
    model = User
    fields = ['username']
    template_name = 'protwo/author_form.html'


class UserUpdate(UpdateView):
    model = User
    fields = ['username']
    template_name = 'protwo/author_form.html'

class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('author-list')
    template_name = 'protwo/author_confirm_delete.html'
    

class ProductList(ListView):
    model = Product   
    template_name = 'protwo/productlist.html'
    
class ProducctDetail(DetailView):
    
    model = Product

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the products
        context['product_list'] = Product.objects.all()
        return context
    template_name = 'protwo/detail.html'
       

def index(request):
    return render(request, 'protwo/index.html')






def user_form(request):
    form = forms.UserForm()
    if request.method =='POST':
        form = forms.UserForm(request.POST)
        if form.isvalid():
            print("validation successs")
            print("name :" +form.cleaned_data['name'])
            print("email :" +form.cleaned_data['email'])
            print(" password :" +form.cleaned_data['password'])
            print("text :" +form.cleaned_data['text'])
    return render(request, 'protwo/user_form.html', {'form': form})

def user_model_form(request):
    form = forms.UserModelForm()
    if request.method =='POST':
        form = forms.UserModelForm(request.POST)
        if form.is_valid():
           form.save(commit=True)
           return index(request)
        else:
            print("invalid page return")   
    return render(request, 'protwo/user_form_model.html', {'form': form})



def card_view(request):
    return render(request, 'protwo/card.html')

def category_view(request):
    return render(request, 'protwo/category.html')


def order_view(request):
    return render(request, '/protwo/order.html')

def product_view(request):
    return render(request, 'protwo/product.html')
     
     
     
     
     