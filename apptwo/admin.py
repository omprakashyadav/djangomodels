from django.contrib import admin
from apptwo.models import Product, Cart, Category,User,Order

# Register your models here.
admin.site.register(Category)
admin.site.register(Cart)
admin.site.register(Product)

admin.site.register(Order)

admin.site.register(User)
